angular.module 'kooskolastamine', ['ngRoute', 'angularFileUpload', 'firebase', 'ui.bootstrap']
  .config ['$locationProvider', '$routeProvider', ($locationProvider, $routeProvider) ->
    $routeProvider.when '/',
      title: 'Esileht'
      controller: 'MainCtrl'
      templateUrl: 'pages/main.html'
    .when '/acceptance',
      title: 'Vastuvõtmine'
      controller: 'AcceptanceCtrl'
      templateUrl: 'pages/acceptance.html'
    .when '/application',
      title: 'Esitamine'
      controller: 'ApplicationCtrl'
      templateUrl: 'pages/application.html'
    .when '/admin',
      title: 'Admin'
      controller: 'AdminCtrl'
      templateUrl: 'pages/admin.html'
    $locationProvider.html5Mode true
  ]
  .service 'Alerts', ->
    class Alerts
      alerts: []
      error: (message) => this.alerts.push type: 'danger', msg: message
      success: (message) => this.alerts.push type: 'success', msg: message
    new Alerts()
  .controller 'NavCtrl', ['$scope', '$location', ($scope, $location) ->
    $scope.isActive = (location) -> location is $location.path()
    $scope.isCollapsed = true
  ]
  .controller 'AuthCtrl', ['Alerts', '$scope', '$firebase', '$firebaseAuth', (Alerts, $scope, $firebase, $firebaseAuth) ->
    # log user in using the Password provider to authenticate
    $scope.authObj = $firebaseAuth new Firebase 'https://kooskolastamine.firebaseio.com'
    $scope.loginWithEmail = (email, password) ->
      $scope.authObj.$authWithPassword
        email: email
        password: password
      .then (authData) ->
        console.log "Logged in as:", authData
        Alerts.success "Logged in as: " + authData.password.email
      .catch (error) ->
        Alerts.error "Authentication failed: " + error

    $scope.logout = -> $scope.authObj.$unauth()
  ]
  .controller 'MainCtrl', ['$scope', '$firebase', '$filter', '$modal', ($scope, $firebase, $filter, $modal) ->
    $scope.objects = $firebase(new Firebase '//kooskolastamine.firebaseio.com/objects').$asArray()
  ]
  .controller 'ModalCtrl', ['$scope', '$modalInstance', 'items', ($scope, $modalInstance, items) ->
    $scope.items = items

    $scope.ok = ->
      $modalInstance.close 'close'
  ]
  .controller 'ApplicationCtrl', ['Alerts', '$scope', '$firebase', 'FileUploader', '$modal', (Alerts, $scope, $firebase, FileUploader, $modal) ->
    $scope.uploader = new FileUploader url: 'http://raxsade.planet.ee/portfolio/php/uploader.php'
    $scope.uploader.onCompleteItem = (fileItem, response, status, headers) ->
      $scope.addDocument response.url, fileItem.file.name

    $scope.openModal = (contents) ->
      console.log 'modal open', contents
      modalInstance = $modal.open
        controller: 'ModalCtrl'
        templateUrl: 'pages/modal.html'
        resolve:
          items: => contents

    $scope.objects = $firebase(new Firebase '//kooskolastamine.firebaseio.com/objects').$asArray()

    $scope.checkedRequirements = (requirements) ->
      a = {}
      for requirement in requirements
        if requirement.checked then a[requirement.$id] = accepted: false
      a
    
    $scope.addObject = (title, description) ->
      if title then $scope.objects.$add title: title, description: description, date: new Date().getTime(), requirements: $scope.checkedRequirements($scope.requirements), documents: $scope.documents
      $scope.newObjectForm.$setPristine()

    $scope.requirements = $firebase(new Firebase '//kooskolastamine.firebaseio.com/requirements').$asArray()

    $scope.documents = []
    $scope.addDocument = (url, title) -> $scope.documents.push url: url, title: title
  ]
  .controller 'AdminCtrl', ['$scope', '$firebase', ($scope, $firebase) ->
    $scope.title = 'Admin'

    $scope.requirements = $firebase(new Firebase '//kooskolastamine.firebaseio.com/requirements').$asArray()
    $scope.addRequirement = (title, description) -> $scope.requirements.$add title: title, description: description
  ]
  .controller 'AcceptanceCtrl', ['$scope', '$firebase', '$modal', ($scope, $firebase, $modal) ->
    $scope.title = 'Acceptance'

    $scope.objects = $firebase(new Firebase '//kooskolastamine.firebaseio.com/objects').$asArray()

    $scope.requirements = $firebase(new Firebase '//kooskolastamine.firebaseio.com/requirements').$asArray()

    $scope.openModal = (contents) ->
      console.log 'modal open', contents
      modalInstance = $modal.open
        controller: 'ModalCtrl'
        templateUrl: 'pages/modal.html'
        resolve:
          items: => contents
  ]
  .controller 'AlertCtrl', ['Alerts', '$scope', (Alerts, $scope) ->
    $scope.alerts = Alerts.alerts
    $scope.addAlert = -> $scope.alerts.push msg: 'Another alert!'
    $scope.closeAlert = (index) -> $scope.alerts.splice index, 1
  ]
  .run ['$location', '$rootScope', ($location, $rootScope) ->
    $rootScope.$on '$routeChangeSuccess', (event, current, previous) ->
      $rootScope.title = current.$$route.title
  ]
