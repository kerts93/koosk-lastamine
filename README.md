# Kooskõlastamise rakendus #
Kooskõlastamine on paljude menetluste osa. Siin on peetud silmas peamiselt avalikku sektorit, kuid kooskõlastamine kui menetlussamm on töövoogude ja juhtimise element loomulikult ka erasektoris.

### Kooskõlastamise olemus ja põhilised sammud: ###
* kooskõlastamisele esitaja
* kooskõlastaja
* kooskõlastamise objekt ehk asi
* kooskõlastamisele esitatav dokumentatsioon
* kooskõlastamise otsus (kooskõlastatud või mitte kooskõlastatud)
* kooskõlastamise nõuded (nõuded, mille täidetust kooskõlastaja kontrollib).

### Prototüüp pakub järgmist funktsionaalsust: ###
* asjade esitamine kooskõlastamisele
* kooskõlastamine kindla nõuete loetelu vastu
* kooskõlastusotsuse tegemine ja avaldamine
* Kooskõlastajaid on mitu, kõik nad teevad otsuse iseseisvalt.

### Süsteemist on näha: ###
* millised asjad on esitatud kooskõlastamisele ja kelle poolt
* kas asjad on juba kooskõlastatud või alles töös
* milline oli kooskõlastamise otsus (kõigi kooskõlastajate lõikes)
* millised nõudeid "feilisid" ja millised olid täidetud.
Kuna see oli päris suur ports funktsionaalsust, siis me ei jõudnud seda tervikuna realiseerida, eriti kasutajate autentimise ja õiguste halduse osas. Aga prototüüp on olemas.

### Kasutatud tehnoloogiad: ###
* AngularJS - front-end raamistik
* Firebase - back-end NoSQL andmebaas + API
* AngularFire - Firebase'i liides AngularJS'i jaoks
* UI Bootstrap - Bootstrap kujundus AngularJS'i jaoks
* Angular File Upload - moodul failide üleslaadimiseks AngularJS'i jaoks
* CoffeeScript - keel, mis kompileeritakse JavaScriptiks
* Sass - CSS laiendus, mis kompileeritakse CSS-ks

### Meeskond:###
* Ragnar Säde
* Kert Sepp
* Ivo Vaabel
* Siim Karoman