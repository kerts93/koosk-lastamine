var express = require('express')
var app = express();

app.set('port', (process.env.PORT || 20141))
app.use(express.static(__dirname + '/'))

app.get('*', function(request, response) {
  // response.send('Hello World!')
  response.sendFile(__dirname + '/index.html')
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})
